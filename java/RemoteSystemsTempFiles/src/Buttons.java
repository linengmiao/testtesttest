
import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Buttons extends JPanel{
	
	private JButton up = new JButton("up");
	private JButton down = new JButton("down");
	private JButton left = new JButton("left");
	private JButton right = new JButton("right");

	public Buttons() {
		this.setLayout(new BorderLayout());
		this.add(up, BorderLayout.PAGE_START);
		this.add(down, BorderLayout.PAGE_END);
		this.add(left, BorderLayout.LINE_START);
		this.add(right, BorderLayout.LINE_END);
		
	}
	
	public JButton getUp() 
	{return up;}

	public JButton getDown() 
	{return down;}

	public JButton getLeft() 
	{return left;}

	public JButton getRight() 
	{return right;}
	
	
	public void upEventListener(ActionListener a){
		up.addActionListener(a);
	}

}
