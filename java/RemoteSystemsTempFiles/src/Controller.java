import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

public class Controller implements ActionListener{
	
	private Tegel[][] tegels= new Tegel[4][4];
	private doolhofView mijndoolhofView;
	private Doolhof mijnDoolHof;
	private Buttons mijnButtons = new Buttons();
	
	
	public void actionPerformed(ActionEvent e)
	{
		Speler tempSpeler = mijnDoolHof.getSpeler();
		

		Coordinaat mijnCoord = tempSpeler.getCoordSpeler();
		ArrayList<Integer> listTemp = mijnCoord.getCoord(); 
		int x = listTemp.get(0);
		int y = listTemp.get(1);
		System.out.printf("huidige coord speler x: %d y: %d\n",x,y);
		
		//wijzig coordinaten van de speler
		if(e.getSource()==mijnButtons.getUp())
		{
			System.out.printf("button up geklikt\n");
			tempSpeler.moveSpeler(1);
		}
		else if(e.getSource()==mijnButtons.getDown())
		{
			System.out.printf("button down geklikt\n");
			tempSpeler.moveSpeler(2);
		}
		else if(e.getSource()==mijnButtons.getLeft())
		{
			System.out.printf("button left geklikt\n");
			tempSpeler.moveSpeler(3);
		}
		else if(e.getSource()==mijnButtons.getRight())
		{
			System.out.printf("button right geklikt/n");
			tempSpeler.moveSpeler(4);
		}
		
		mijnDoolHof.setSpeler(tempSpeler); //ken de gemodificeerde coordinaten toe aan de speler
		mijnDoolHof.modifyDoolHof(tempSpeler.getCoordSpeler()); // geef de nieuwe coord vd speler mee voor het herschilderen vh doolhof
		//mijnDoolHof.updateDoolhof();
		this.workAroundUIRepaint();
	}
	
	public void workAroundUIRepaint()
	{
		Container tempC = mijndoolhofView.getContainer();
		tempC.revalidate();
		tempC.repaint();
		
	}
	
/*******************************************************************************************************/
	public Controller()
	{
		mijnDoolHof = new Doolhof(3); //hier set je de grootte vh doolhof, moet min 3 zijn
		mijndoolhofView = new doolhofView(this, mijnDoolHof, mijnButtons);	
	}

	public Tegel[][] getTegels() {
		return tegels;
	}

	public void setTegels(Tegel[][] tegels) {
		this.tegels = tegels;
	}
	
	public static void main(String[] args)
	{
		Controller mijnCtrller = new Controller();
	}
}
