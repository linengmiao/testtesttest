/*
 * Alexandre Umba SPE
 * 
 */

import java.util.ArrayList;

public class Coordinaat {
	
	ArrayList<Integer> Coord = new ArrayList<>();
	
	public Coordinaat(int x, int y)
	{
		ArrayList<Integer> listTemp = new ArrayList<>();
		listTemp.add(x);
		listTemp.add(y);
		setCoord(listTemp);
	}
	
	public static Coordinaat random(int size) {
		return new Coordinaat((int) (((Math.random() * 10) / size)), (int) (((Math.random() * 10) / size)));
	}
	
	public Coordinaat()
	{	
		ArrayList<Integer> listTemp = new ArrayList<>();
		listTemp.add(1);
		listTemp.add(1);
	}
	
	public ArrayList<Integer> getCoord() {
		return Coord;
	}

	//function overloading
	public void setCoord(int x, int y) {
		ArrayList<Integer> listTemp = new ArrayList<>();
		listTemp.add(x);
		listTemp.add(y);
		setCoord(listTemp);
	}
	
	public void setCoord(ArrayList<Integer> list) {
		this.Coord = list;
	}

}
