/*
 * Alexandre Umba SPE
 * 
 */

import java.util.ArrayList;
import java.util.Collections;

public class Doolhof {

	private int grootte;
	private Tegel[][] doolhof;
	private int sizeDoolhof;
	private Speler mijnSpeler = new Speler();
	
	public Doolhof(int doolhofSize)
	{
		setSizeDoolhof(doolhofSize);
		createDoolhof();		
	}
	
	/*public void updateDoolhof()
	{
	
		Coordinaat tempCoord = mijnSpeler.getCoordSpeler();
		this.modifyDoolHof(tempCoord);
	}*/
	
	public void modifyDoolHof(Coordinaat modCoord)
	{
		Tegel tempTegel = new Tile_T(); //om maar iets toe te kennen...
		ArrayList<Integer> listTemp = modCoord.getCoord();
		int x = listTemp.get(0);
		int y = listTemp.get(1);
		
		System.out.printf("nieuwe coord speler x: %d y: %d\n",x,y);
				
		tempTegel = this.getTegel((x/this.getSizeDoolhof()),(y/this.getSizeDoolhof()));
		
		listTemp.set(0, x % this.getSizeDoolhof());
		listTemp.set(0, y % this.getSizeDoolhof());
		Coordinaat mijnCoord = new Coordinaat();
		mijnCoord.setCoord(listTemp);
		
		tempTegel.setVakje(mijnCoord, 2); // is de waarde vr speler
	//	System.out.print("nieuwe coord speler: ");
		
	}
	
	public Tegel[][] createDoolhof()
	{
		int doolhofSize = getSizeDoolhof();
		Tegel[][] doolhof = new Tegel[doolhofSize * doolhofSize][doolhofSize * doolhofSize];

		ArrayList<Tegel> doolhofList = new ArrayList<Tegel>();
		
		for (int i = 0; i < ((doolhofSize * doolhofSize)/4); i++) {
			doolhofList.add(new Tile_T());
			doolhofList.add(new Tile_Bend());
			doolhofList.add(new Tile_Cross());
			doolhofList.add(new Tile_Straight());
		}
		
		doolhof = shuffleDoolhof(doolhofList);	
		setMijnDoolhof(doolhof);
		doolhof = plaats(doolhof,2); // 
		doolhof = plaats(doolhof,3); 
		
		
		return doolhof;
	}
	
	public Tegel[][] plaats(Tegel[][] doolhof, int objToPlace)
	{
		Tegel newTegel = new Tile_Straight();
		
		//get rand coord
		Coordinaat mijnCoord = new Coordinaat(0,0);
		mijnCoord = mijnCoord.random(this.getSizeDoolhof()); //return rand coordinaat om goud te plaatsen
		int testValue = this.getSizeDoolhof(); 
		
		//get x en y uit coord --te hercoderen
		ArrayList<Integer> listTemp = mijnCoord.getCoord();
		int x = listTemp.get(0);
		int y = listTemp.get(1);
		
		newTegel = this.getTegel(x,y);
		
		newTegel.setVakje(mijnCoord, objToPlace);
		
		if(objToPlace == 2) //als de speler dient geplaatst te worden
		{mijnSpeler.setCoordSpeler(mijnCoord);}
	
		this.setTegel(x, y, newTegel);
		
		return doolhof;
	}
	
	public Tegel[][] shuffleDoolhof(ArrayList<Tegel>doolhofList)
	{
		int doolhofSize = getSizeDoolhof();
		Tegel[][]doolhofArray = new Tegel[doolhofSize][doolhofSize];
		int huidigeMod=1;
		int j=0;
		int n=0;
		int vorigeMod = 0%getSizeDoolhof();

		for(int k = 0;k <doolhofSize;k++)
		{
			for(int l=0;l<doolhofSize;l++)
			{	
				doolhofArray[k][l] = new Tile_T();
			}					
		}		
		
		Collections.shuffle(doolhofList);
		int listSize = doolhofList.size();
		
		//list omvormen tot 2d array
		for(int i=0;i<listSize;i++)
		{	 
			if(i!=0) //bij de eerste iteratie is dit statement sowieso true en dan kloppen mijn lussen ni meer
			{huidigeMod = (i+1)%getSizeDoolhof();} //+1 daar het zero based is			
			n++;
			if(huidigeMod == 0)
			{j++;
			n = 0;
			vorigeMod = huidigeMod;}
			doolhofArray[j][n].setTegel(doolhofList.get(i));

		}
		
	
		return doolhofArray;
	}
	
	public Tegel[][] getMijndoolhof() {
		return doolhof;
	}
	
	public Tegel getTegel(int x, int y) {

		//om een duistere reden gaat deze soms out of bounds. Herrunnen
		
		return doolhof[x][y];
	}

	public void setTegel(int x, int y, Tegel newTegel) {
		doolhof[x][y] = newTegel;
	}
	
	public void setMijnDoolhof(Tegel[][] mijndoolhof) {
		this.doolhof = mijndoolhof;
	}
	
	public int getSizeDoolhof() {
		return sizeDoolhof;
	}

	public void setSizeDoolhof(int sizeDoolhof) {
		this.sizeDoolhof = sizeDoolhof;
	}
	
	public void moveTo()
	{
		//controle of move ok is. Anders hier exception throwen 	
	}
	
	public Speler getSpeler()
	{return mijnSpeler;}
	
	public void setSpeler(Speler tempSpeler)
	{this.mijnSpeler = tempSpeler;}
	
	
}
