import java.util.ArrayList;

public class Speler {

	private int score;
	private Coordinaat mijnCoord= new Coordinaat(0,0); 

	public Speler()
	{
	}
	
	public void setCoordSpeler(Coordinaat newCoord)
	{
		this.mijnCoord = newCoord;
	}
	
	public Coordinaat getCoordSpeler()
	{return mijnCoord;}
	
	public void moveSpeler(int move)
	{
		ArrayList<Integer> curCoord = new ArrayList<>();
		curCoord = mijnCoord.getCoord();
		if(move==1) //up
		{
			curCoord.set(0, (curCoord.get(0) - 1));
		}
		else if(move==2) //down
		{
			curCoord.set(0, (curCoord.get(0) + 1));
		}
		else if(move==3) //down
		{
			curCoord.set(1, (curCoord.get(1) - 1));
		}
		else if(move==4) //down
		{
			curCoord.set(1, (curCoord.get(1) + 1));
		}
		
		mijnCoord.setCoord(curCoord);
		
	}
}
