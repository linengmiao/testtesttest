/*
 * Alexandre Umba SPE
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;


public abstract class Tegel {

	private Vakje[][] vakjes= new Vakje[3][3];
	private int rotatie;
	
	
	public Tegel()
	{
		//sublcasse zal juiste waarde in vak zetten
	}
	
	public String toString() {
		return "Tegel [vakjes=" + Arrays.toString(vakjes) + "]";
	}

	public void setTegel(Tegel tegel)
	{
		Coordinaat mijnCoord = new Coordinaat(0,0);
		
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				mijnCoord.setCoord(i, j);
				this.setVakje(mijnCoord, tegel.getVakjeInt((mijnCoord)));
			}
		}		
	}
	
	public Vakje getVakje(int x, int y)
	{
		return vakjes[x][y];
	}
			
	public void setTegel(Vakje[][] vakjes) {
		this.vakjes = vakjes;
	}
	
	public Vakje[][] getTegel() {
		return vakjes;
	}
	
	public Vakje getVakje(Coordinaat coordVakje)
	{
		ArrayList<Integer> listTemp = coordVakje.getCoord();
		return vakjes[listTemp.get(0)][listTemp.get(1)];
	}
	
	public void rotate(Coordinaat mijnCoord) {
		Vakje[][] arrVakjeTemp = new Vakje[3][3];
		//Coordinaat mijnCoord = new Coordinaat(0,0);
		
		int n = 3;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				//arrVakjeTemp[i][j] = getVakje(mijnCoord)[n - 1 - j][i];
			}
		}
		setTegel(arrVakjeTemp);
	}
	
	public int getVakjeInt(Coordinaat coordVakje)
	{
		int hulpVar;
		ArrayList<Integer> listTemp = coordVakje.getCoord();
		hulpVar = (vakjes[listTemp.get(0)][listTemp.get(1)].getInhoud());
		return hulpVar;
	}
	
	public void setVakje(Coordinaat coordVakje, int value)
	{
		ArrayList<Integer> listTemp = coordVakje.getCoord();
		this.vakjes[listTemp.get(0)][listTemp.get(1)].setInhoud(value);
	}
	
	public void plaatsGoud(Coordinaat coordVakje)
	{
		ArrayList<Integer> listTemp = coordVakje.getCoord();
		vakjes[listTemp.get(0)][listTemp.get(1)].setInhoud(3);
	}
	
	public static void main(String[] args)
	{
		
		Vakje[][] vakjes= new Vakje[3][3];
		int waarde;
		
		Tegel mijnTegel =new Tile_T();
		
		vakjes = mijnTegel.getTegel();
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				waarde = vakjes[i][j].getInhoud();
			}
		}
		
	}
}
