/*
 * Alexandre Umba SPE
 * 
 */

public class Tile_Cross extends Tegel{

	public Tile_Cross()
	{
		Vakje vakjes[][] ={{new Vakje(1) , new Vakje(0), new Vakje(1)},
							{new Vakje(0) , new Vakje(0), new Vakje(0)},
							{new Vakje(1) , new Vakje(0), new Vakje(1)}};
		
		setTegel(vakjes);
	}
	
	
}
