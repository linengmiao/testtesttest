/*
 * Alexandre Umba SPE
 * 
 */

public class Vakje{
	
	private int inhoud;
	


	public String getInhoudStr() {
		
		return String.valueOf(inhoud);
	}

	public static final int LEEG = 1;
	public static final int MUUR = 2;
	public static final int GOUD = 3;
	public static final int MAN = 4;
	
	public Vakje(int value)
	{setInhoud(value);}
	
	
	public int getInhoud() {
		return inhoud;
	}

	public void setInhoud(int inhoud) {
		this.inhoud = inhoud;
	}
	
	public boolean isLeeg()
	{
		if (getInhoud() == LEEG)
		{return true;}
		else
		{return false;}
	}
	
	public boolean isMuur()
	{
		if (getInhoud() == MUUR)
		{return true;}
		else
		{return false;}
		
	}
	
	public boolean isGoud()
	{
		if (getInhoud() == GOUD)
		{return true;}
		else
		{return false;}
		
	}
	
	public boolean isMan()
	{
		if (getInhoud() == MAN)
		{return true;}
		else
		{return false;}
		
	}
	
	
	}
