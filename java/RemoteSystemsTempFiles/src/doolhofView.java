import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.* ;
import javax.swing.*;
import java.awt.event.ActionListener;

import javax.imageio.*;


public class doolhofView extends JFrame{
	
	private Container c;	
	
	public doolhofView(Controller ctrl, Doolhof mijnDoolhof, Buttons mijnButtons)
	{	 
		Tegel[][] tempTegel = new Tegel[mijnDoolhof.getSizeDoolhof()][mijnDoolhof.getSizeDoolhof()];  //hulp variabele 
		Vakje [][]tempVakje = new Vakje[3][3];
		tegelView[][] tegels= new tegelView[mijnDoolhof.getSizeDoolhof()][mijnDoolhof.getSizeDoolhof()]; //elke tegel krijgt zijn eigen view
		
		GridLayout gl = new GridLayout(mijnDoolhof.getSizeDoolhof(),mijnDoolhof.getSizeDoolhof());
		this.setLayout(gl);

		
		for(int i=0;i<mijnDoolhof.getSizeDoolhof();i++)
		{
			for(int j=0;j<mijnDoolhof.getSizeDoolhof();j++)
			{ 
				tempTegel[i][j]= mijnDoolhof.getTegel(i, j); // ik get de 9 tegels vh doolhof
				tegels[i][j] = new tegelView(tempTegel[i][j]); //returnt JPanel
				this.add(tegels[i][j]);
			}
		}
		
		
		c = getContentPane();	
		c.add(mijnButtons, BorderLayout.PAGE_END);
		mijnButtons.getUp().addActionListener(ctrl);
		mijnButtons.getDown().addActionListener(ctrl);
		mijnButtons.getLeft().addActionListener(ctrl);
		mijnButtons.getRight().addActionListener(ctrl);
		
		//c.setLayout(new FlowLayout()); //BELEMMERT DE GRIDLAYOUT!!

		setSize(500, 500);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}

	
	public Container getContainer()
	{return c;}
	
	public static void main(String[] args)
	{
		//	doolhofView mijnDoolhofView = new doolhofView();
	}
}
