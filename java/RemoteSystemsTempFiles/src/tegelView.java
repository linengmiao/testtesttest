/*
 * Alexandre Umba SPE
 * 
 */

import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.* ;

import javax.swing.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Graphics;

public class tegelView extends JPanel{

	
	private JPanel kleinPaneel = new JPanel();
	private JLabel[][] lblVakje= new JLabel[3][3];	
	private Vakje[][] vakjes= new Vakje[3][3];
	private BufferedImage imgWall = null;
	private BufferedImage imgPlayer = null;
	private BufferedImage imgGold = null;

	

	public tegelView(Tegel tegelObj)
	{	
		VakjeView[][] vakjes= new VakjeView[3][3];
		int[][] tempVakjes = new int[3][3]; 
		int waarde;
		String vakInhoud;
		  
		Coordinaat mijnCoordinaat = new Coordinaat(0,0);
		Graphics gWall=null;
		Graphics gPlayer=null;
		Graphics gGold=null;
		ImageIcon iconWall = null;
		ImageIcon iconPlayer = null;
		ImageIcon iconGold = null;
		
		try 
		{
		    imgWall = ImageIO.read(new File("C:/Users/Yalishand/Downloads/wall.jpg")); 
		    iconWall = new ImageIcon(imgWall);
		    gWall = imgWall.getGraphics();
		    
		    imgPlayer = ImageIO.read(new File("C:/Users/Yalishand/Downloads/fotoVnMij.jpg")); 
		    iconPlayer = new ImageIcon(imgPlayer);
		    gPlayer = imgPlayer.getGraphics();
		    
		    imgGold = ImageIO.read(new File("C:/Users/Yalishand/Downloads/gold.jpg")); 
		    iconGold = new ImageIcon(imgGold);
		    gGold = imgGold.getGraphics();
		    
		    
		} 
		catch (IOException e) 
		{
			System.out.print("FOUT file niet gevonden/n");
		    e.printStackTrace();
		}
		
		
		for(int k=0;k<3;k++)
		{
			for(int l=0;l<3;l++)
			{
				mijnCoordinaat.setCoord(k,l);
				tempVakjes[k][l] = tegelObj.getVakjeInt(mijnCoordinaat);
			}
		}

		setLayout(new GridLayout(3,3));
		this.setLayout(new GridLayout(3, 3));
		
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				
				lblVakje[i][j] = new JLabel(" ");
				vakInhoud = String.valueOf(tempVakjes[i][j]); 
				if (vakInhoud.equals("0"))
				{
					//vakInhoud = "#";
					//gWall.drawImage(imgWall,i,j,10,10,null);
					lblVakje[i][j].setIcon(iconWall);
				}
				
				else if (vakInhoud.equals("1"))
				{
					vakInhoud = " ";
					lblVakje[i][j].setText(String.valueOf(vakInhoud));
				}
				else if(vakInhoud.equals("2"))
				{
					//gPlayer.drawImage(imgPlayer,i,j,40,40,null);
					lblVakje[i][j].setIcon(iconPlayer);
				}
				
				else if(vakInhoud.equals("3"))
				{
					//gGold.drawImage(imgGold,i,j,55,55,null);
					lblVakje[i][j].setIcon(iconGold);
				}
					
				add(lblVakje[i][j]);
			}
		}
		
		setSize(300,200);
		setVisible(true);
	//	setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public static void main(String[] args)
	{
	
	//	Tegel mijnTegel = new Tile_Cross();//
	//	tegelView mijnTegelView = new tegelView(mijnTegel);

	}
	
	
}
